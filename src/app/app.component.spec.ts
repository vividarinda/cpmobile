import { async, TestBed } from '@angular/core/testing';
import { MyApp } from './app.component';
import { TestUtils } from '../../test-config/test-utils';

describe('MyApp Component', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestUtils.configureIonicTestingModule();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyApp);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component instanceof MyApp).toBe(true);
  });

  it('should be 2', () => {
    expect(component.onePlusOne()).toBe(10);
  });
});