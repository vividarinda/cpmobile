import { Component } from '@angular/core';
import { Platform, App, Config } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { ModalEnterFadeIn, ModalLeaveFadeOut } from '../animations/animations';
import { LandingPage } from '../pages/landing/landing.component';
import { TabsPage } from '../pages/tabs/tabs';
import { DimensionService } from '../services/dimension.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LandingPage;

  constructor(platform: Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen, 
              private push: Push, 
              public app: App, 
              private androidFullScreen: AndroidFullScreen,
              private config: Config,
              private dimensionService: DimensionService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.dimensionService.setScreenWidth(platform.width());
      this.dimensionService.setScreenHeight(platform.height());

      if(platform.is("ios")){
        this.androidFullScreen.isImmersiveModeSupported().then(
          () => {
            this.androidFullScreen.showUnderStatusBar();
          }
        );
      }
      statusBar.styleBlackTranslucent();
      splashScreen.hide();
    });

    const options: PushOptions = {
      android: {
        topics: ['news']
      },
      ios: {
          alert: 'true',
          badge: true,
          sound: 'false',
          topics: ['news']
      },
      windows: {},
      browser: {
          pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
   };
   
   this.setCustomTransition();

   const pushObject: PushObject = this.push.init(options);
   
   pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
   
   pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
   
   pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }

  private setCustomTransition(){
    this.config.setTransition('enter-fade', ModalEnterFadeIn);
    this.config.setTransition('leave-fade', ModalLeaveFadeOut);
  }
}
