import { DataService } from './../services/data.service';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { GoogleMaps } from '@ionic-native/google-maps';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { TokenInterceptor } from '../interceptors/token.interceptor';
import { ExpandableHeaderModule } from '../components/expandable-header/expandable-header.module';
import { AlertService } from '../services/alert.service';
import { LoadingService } from '../services/loading.service';
import { HomePageModule } from '../pages/home/home.component.module';
import { SnackbarService } from '../services/snackbar.service';
import { Push } from '@ionic-native/push';
import { MyOrdersPageModule } from '../pages/my-orders/my-orders.component.module';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { LandingPageModule } from '../pages/landing/landing.component.module';
import { DimensionService } from '../services/dimension.service';
import { MyEquipmentsPageModule } from '../pages/my-equipments/my-equipments.component.module';
import { TabsParamService } from '../services/tabsparam.service';

@NgModule({
  declarations: [
    MyApp,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp, {pageTransition: 'transition-ios'}),
    HttpClientModule,
    ExpandableHeaderModule,
    LandingPageModule,
    HomePageModule,
    MyOrdersPageModule,
    MyEquipmentsPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    DataService,
    NativePageTransitions,
    AlertService,
    LoadingService,
    SnackbarService,
    Push,
    GoogleMaps,
    AndroidFullScreen,
    DimensionService,
    TabsParamService
  ]
})
export class AppModule {}
