import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
 
import { Observable } from 'rxjs';
import { _throw } from 'rxjs/observable/throw';
 
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
 
    constructor(private storage: Storage) { }
 
    // Intercepts all HTTP requests!
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> { 
        let promise = this.storage.get('my_token');
        let login = 'login';
        let register = 'register';
        let clonedReq = request;

        return Observable.fromPromise(promise)
            .mergeMap(token => {
                if(request.url.search(login) == -1 && request.url.search(register) == -1){
                    clonedReq = this.addToken(request, token);
                }
                return next.handle(clonedReq);
            });
    }
 
    private addToken(request: HttpRequest<any>, token: any) {
        if (token) {
            let clone: HttpRequest<any>;
            clone = request.clone({
                setHeaders: {
                    Accept: `application/json`,
                    'Content-Type': `application/json`,
                    Authorization: `Bearer ${token}`
                }
            });

            return clone;
        }
 
        return request;
    }
}