export const CONSTANTS = {
  API: {
    // BASE_URL: 'https://moqmulatorv2.azurewebsites.net/',
    BASE_URL: 'http://138.91.37.22/',
    TIME_OUT_VAL: 5000
  },
  LOGO: {
    ALL: 'allEquipments.png',
    SCANIA: 'logoScania.png',
    UDTRUCK: 'udTrucksLogo.png',
    BOMAG: 'bomagLogo.png',
    KOMATSU: 'logoKomatsu.png',
    TADANO: 'tadanoCompanyLogo.png',
    OTHERS: 'others.png'
  },
  ERROR: {
    '0': 'Network Error'
  },
  STORAGE: {
    KEY: '3bec3a45b4981a6ffb56dd8e84510f51'
  }
}
