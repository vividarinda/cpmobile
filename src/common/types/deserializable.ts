export interface Deserializable{
  deserialize(jsonInput: Object): this;
}
