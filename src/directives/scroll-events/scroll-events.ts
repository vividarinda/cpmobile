import { Content } from 'ionic-angular';
import { Directive, ElementRef, Input, Renderer2, SimpleChanges } from '@angular/core';


@Directive({
    selector: '[scrollHide]'
})
export class ScrollHideDirective {

    @Input('scrollHide') config: ScrollEventConfig;
    @Input('scrollContent') scrollContent: Content;

    contentHeight: number;
    scrollHeight: number;
    lastScrollPosition: number;
    lastValue: number = 0;

    constructor(private element: ElementRef, private renderer: Renderer2) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.scrollContent && this.config) {
            this.scrollContent.ionScrollStart.subscribe((ev) => {
                this.contentHeight = this.scrollContent.getScrollElement().offsetHeight;
                this.scrollHeight = this.scrollContent.getScrollElement().scrollHeight;
                if (this.config.maxValue === undefined) {
                    this.config.maxValue = this.element.nativeElement.offsetHeight;
                }
                this.lastScrollPosition = ev.scrollTop;
            });
            this.scrollContent.ionScroll.subscribe((ev) => this.adjustElementOnScroll(ev));
            this.scrollContent.ionScrollEnd.subscribe((ev) => this.adjustElementOnScroll(ev));
        }
    }

    private adjustElementOnScroll(ev) {
        if (ev) {
            ev.domWrite(() => {
                let scrollTop: number = ev.scrollTop > 0 ? ev.scrollTop : 0;
                let scrolldiff: number = scrollTop - this.lastScrollPosition;
                this.lastScrollPosition = scrollTop;
                let newValue = this.lastValue + scrolldiff * 1;
                if(this.lastScrollPosition === 0) {
                  newValue = Math.max(0, Math.min(0, this.config.maxValue));
                } else if(scrolldiff < 0){
                  newValue = Math.max(0, Math.min(newValue, this.config.maxValue));
                }else{
                  newValue = Math.max(0, Math.min(newValue, 0.7 * this.config.maxValue));
                }
                this.renderer.setStyle(this.element.nativeElement, this.config.cssProperty, `-${newValue}px`);
                this.lastValue = newValue;
            });
        }
    }
}

@Directive({
    selector: '[scrollTranslate]'
})
export class ScrollTranslateDirective {

    @Input('scrollTranslate') config: ScrollEventConfig;
    @Input('scrollContent') scrollContent: Content;

    contentHeight: number;
    scrollHeight: number;
    lastScrollPosition: number;
    lastValue: number = 0;

    constructor(private element: ElementRef, private renderer: Renderer2) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.scrollContent && this.config) {
            this.scrollContent.ionScrollStart.subscribe((ev) => {
                this.contentHeight = this.scrollContent.getScrollElement().offsetHeight;
                this.scrollHeight = this.scrollContent.getScrollElement().scrollHeight;
                if (this.config.maxValue === undefined) {
                    this.config.maxValue = this.element.nativeElement.offsetHeight;
                }
                this.lastScrollPosition = ev.scrollTop;
            });
            this.scrollContent.ionScroll.subscribe((ev) => this.adjustElementOnScroll(ev));
            this.scrollContent.ionScrollEnd.subscribe((ev) => this.adjustElementOnScroll(ev));
        }
    }

    private adjustElementOnScroll(ev) {
        if (ev) {
            ev.domWrite(() => {
                let scrollTop: number = ev.scrollTop > 0 ? ev.scrollTop : 0;
                let scrolldiff: number = scrollTop - this.lastScrollPosition;
                this.lastScrollPosition = scrollTop;
                let newValue = this.lastValue + scrolldiff * 0.5;
                if(this.lastScrollPosition === 0) {
                  newValue = Math.max(0.05 * this.config.maxValue, Math.min(0, this.config.maxValue));
                }else{
                  newValue = Math.max(0.05 * this.config.maxValue, Math.min(newValue, 0.2 * this.config.maxValue));
                }
                this.renderer.setStyle(this.element.nativeElement, this.config.cssProperty, `${newValue}px`);
                this.lastValue = newValue;
            });
        }
    }
}

@Directive({
    selector: '[scrollOpacity]'
})
export class ScrollOpacityDirective {

    @Input('scrollOpacity') config: ScrollEventConfig;
    @Input('scrollContent') scrollContent: Content;

    contentHeight: number;
    scrollHeight: number;
    lastScrollPosition: number;
    lastValue: number = 0;

    constructor(private element: ElementRef, private renderer: Renderer2) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.scrollContent && this.config) {
            this.scrollContent.ionScrollStart.subscribe((ev) => {
                this.contentHeight = this.scrollContent.getScrollElement().offsetHeight;
                this.scrollHeight = this.scrollContent.getScrollElement().scrollHeight;
                if (this.config.maxValue === undefined) {
                    this.config.maxValue = this.element.nativeElement.offsetHeight;
                }
                this.lastScrollPosition = ev.scrollTop;
            });
            this.scrollContent.ionScroll.subscribe((ev) => this.adjustElementOnScroll(ev));
            this.scrollContent.ionScrollEnd.subscribe((ev) => this.adjustElementOnScroll(ev));
        }
    }

    private adjustElementOnScroll(ev) {
        if (ev) {
            ev.domWrite(() => {
                let scrollTop: number = ev.scrollTop > 0 ? ev.scrollTop : 0;
                let scrolldiff: number = scrollTop - this.lastScrollPosition;
                this.lastScrollPosition = scrollTop;
                let newValue = this.lastValue + scrolldiff * 0.01;
                if(this.lastScrollPosition === 0) {
                  newValue = Math.max(0, Math.min(0, 1));
                }else{
                  newValue = Math.max(0, Math.min(newValue, 1));
                }
                this.renderer.setStyle(this.element.nativeElement, this.config.cssProperty, `${newValue}`);
                this.lastValue = newValue;
            });
        }
    }
}

@Directive({
    selector: '[scrollDisplay]'
})
export class ScrollDisplayDirective {

    @Input('scrollDisplay') config: ScrollEventConfig;
    @Input('scrollContent') scrollContent: Content;

    contentHeight: number;
    scrollHeight: number;
    lastScrollPosition: number;
    lastValue: number = 0;

    constructor(private element: ElementRef, private renderer: Renderer2) {
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.scrollContent && this.config) {
            this.scrollContent.ionScrollStart.subscribe((ev) => {
                this.contentHeight = this.scrollContent.getScrollElement().offsetHeight;
                this.scrollHeight = this.scrollContent.getScrollElement().scrollHeight;
                if (this.config.maxValue === undefined) {
                    this.config.maxValue = this.element.nativeElement.offsetHeight;
                }
                this.lastScrollPosition = ev.scrollTop;
            });
            this.scrollContent.ionScroll.subscribe((ev) => this.adjustElementOnScroll(ev));
            this.scrollContent.ionScrollEnd.subscribe((ev) => this.adjustElementOnScroll(ev));
        }
    }

    private adjustElementOnScroll(ev) {
        if (ev) {
            ev.domWrite(() => {
                let scrollTop: number = ev.scrollTop > 0 ? ev.scrollTop : 0;
                let scrolldiff: number = scrollTop - this.lastScrollPosition;
                this.lastScrollPosition = scrollTop;
                let newValue = 'none';
                if(this.lastScrollPosition === 0) {
                  newValue = 'none';
                } else if(scrolldiff < 0){
                  if(this.lastScrollPosition < this.config.maxValue){
                    newValue = 'none';
                  }
                }else{
                  if(this.lastScrollPosition > this.config.maxScrollDistance * this.config.maxValue){
                    newValue = this.config.newValue ? this.config.newValue: 'block';
                  }
                }
                this.renderer.setStyle(this.element.nativeElement, this.config.cssProperty, newValue);
            });
        }
    }
}

export interface ScrollEventConfig {
    cssProperty: string;
    maxValue: number;
    maxScrollDistance?: number;
    newValue?: string;
}
