import { NgModule } from '@angular/core';
import { ScrollHideDirective, ScrollTranslateDirective, ScrollOpacityDirective, ScrollDisplayDirective } from './scroll-events/scroll-events';
import { CommonModule } from '../../node_modules/@angular/common';
@NgModule({
	declarations: [
		ScrollHideDirective,
		ScrollTranslateDirective,
		ScrollOpacityDirective,
		ScrollDisplayDirective
	],
	imports: [CommonModule],
	exports: [
		ScrollHideDirective,
		ScrollTranslateDirective,
		ScrollOpacityDirective,
		ScrollDisplayDirective
	]
})
export class DirectivesModule {}
