import { IonicModule } from 'ionic-angular';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ExpandableHeaderComponent } from './expandable-header';

@NgModule({
	declarations: [ExpandableHeaderComponent],
	imports: [IonicModule],
    exports: [ExpandableHeaderComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExpandableHeaderModule {}
