import { TimelineComponent } from './timeline';
import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';

@NgModule({
	declarations: [
    TimelineComponent
  ],
	imports: [IonicModule],
	exports: [
    TimelineComponent
  ]
})
export class TimelineModule {}
