import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MyEquipmentsStatusUnitPage } from '../../pages/my-equipments/my-equipments-status/my-equipments-status-unit/my-equipments-status-unit.component';

@Component({
  selector: 'notification',
  templateUrl: 'notification.html'
})
export class NotificationComponent {
  @Input('endIcon') endIcon = "ionic";
  @Input('items') items = [];
  constructor(private navCtrl: NavController) {

  }

  public onClickUnitDetail(equipmentId: string){
    this.navCtrl.push(MyEquipmentsStatusUnitPage, {equipmentId: equipmentId});
  }
}
