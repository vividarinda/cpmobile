import { NotificationComponent } from './notification';
import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';

@NgModule({
	declarations: [
    NotificationComponent
  ],
	imports: [IonicModule],
	exports: [
    NotificationComponent
  ]
})
export class NotificationModule {}
