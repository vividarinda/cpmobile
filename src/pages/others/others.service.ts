import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OthersService {

  constructor(private http: HttpClient) {
    console.log('Hello RestProvider Provider');
  }
}
