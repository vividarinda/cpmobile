import { Deserializable } from './../../common/types/deserializable';

export class OthersModel implements Deserializable{
  tickets: number;
  orders: number;

  deserialize(objectJson: Object): this{
    Object.assign(this, objectJson);
    return this;
  }
}
