import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OthersPage } from './others.component';
import { OthersService } from './others.service';

@NgModule({
  declarations: [
    OthersPage
  ],
  imports: [
    IonicPageModule.forChild(OthersPage)
  ],
  providers: [
    OthersService
  ]
})
export class MyTicketsPageModule {}
