import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { OthersService } from './others.service';
import { OthersModel } from './others.model';

@IonicPage()
@Component({
  selector: 'page-others',
  templateUrl: 'others.component.html',
  providers: [OthersService]
})
export class OthersPage {

  public model = new OthersModel();

  constructor(public navCtrl: NavController, private othersService: OthersService) {
    
  }
}
