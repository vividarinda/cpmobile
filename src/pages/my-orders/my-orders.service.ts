import { CONSTANTS } from './../../common/constants/constants';
import { Injectable } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyOrdersService {
  constructor(private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }

  public getOrderSummary(customerCode: string): Observable<any>{
    return this.dataService.get(`http://moqmulatorv2.azurewebsites.net/api/Orders/OrderSummaryForMyOrder?customerCode=${customerCode}`);
  }

  public getOpenOrderList(customerCode: string, soNumber = '', poCustomer = '', partNumber = '', partDescription = ''): Observable<any>{
    return this.dataService.get(
      `http://moqmulatorv2.azurewebsites.net/api/Orders/OpenOrder?customerCode=${customerCode}&soNumber=${soNumber}&poCustomer=${poCustomer}&partNumber=${partNumber}&partDescription=${partDescription}`);
  }

  public getClosedOrderList(customerCode: string, soNumber = '', poCustomer = '', partNumber = '', partDescription = ''): Observable<any>{
    return this.dataService.get(
      `http://moqmulatorv2.azurewebsites.net/api/Orders/ClosedOrder?customerCode=${customerCode}&soNumber=${soNumber}&poCustomer=${poCustomer}&partNumber=${partNumber}&partDescription=${partDescription}`);
  }
}