import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
@Component({
  selector: 'my-orders-filter',
  templateUrl: 'my-orders-filter.component.html'
})
export class MyOrdersFilter {

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
  }

  public onClickCloseButton(){
    this.viewCtrl.dismiss();
  }

  public onClickApply(){
    this.viewCtrl.dismiss();
  }

}
