import { MyOrdersService } from './my-orders.service';
import { Component, ViewChild } from '@angular/core';
import { NavController, IonicPage, Content, ModalController } from 'ionic-angular';
import { MyOrdersModel, ClosedOrder } from './my-orders.model';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { LoadingService } from '../../services/loading.service';
import { MyOrdersOpenOrderPage } from './my-orders-open-order/my-orders-open-order.component';
import { DimensionService } from '../../services/dimension.service';
import { ScrollEventConfig } from '../../directives/scroll-events/scroll-events';
import { TabsPage } from '../tabs/tabs';
import * as moment from 'moment';
import { MyOrdersClosedOrderPage } from './my-orders-closed-order/my-orders-closed-order.component';
import { MyOrdersSort } from './my-orders-sort/my-orders-sort.component';
import { MyOrdersEmail } from './my-orders-email/my-orders-email.component';
import { MyOrdersFilter } from './my-orders-filter/my-orders-filter.component';

@IonicPage()
@Component({
  selector: 'page-my-orders',
  templateUrl: 'my-orders.component.html',
  providers: [MyOrdersService]
})
export class MyOrdersPage {

  @ViewChild(Content) content: Content;
  public model = new MyOrdersModel();
  public viewportWidth: number;

  public headerScrollConfig: ScrollEventConfig = { cssProperty: 'margin-top', maxValue: 200 };
  public translateScrollConfig: ScrollEventConfig = { cssProperty: 'padding-left', maxValue: 10 };
  public opacityScrollConfig: ScrollEventConfig = { cssProperty: 'opacity', maxValue: 1 };
  public rating = [0,1,2,3,4];
  public displayScrollConfig: ScrollEventConfig = { cssProperty: 'display', maxValue: 1 , maxScrollDistance: 0.7};

  public fabDisplay = false;


  constructor(public navCtrl: NavController, 
              private myOrdersService: MyOrdersService,
              private nativePageTransitions: NativePageTransitions,
              private loadingService: LoadingService,
              public dimensionService: DimensionService,
              private modalCtrl: ModalController) {
    this.headerScrollConfig  = { cssProperty: 'margin-top', maxValue: dimensionService.getScreenWidth() };
    this.translateScrollConfig = { cssProperty: 'padding-left', maxValue: dimensionService.getScreenWidth() };
    this.opacityScrollConfig = { cssProperty: 'opacity', maxValue: 1 };
    this.displayScrollConfig = { cssProperty: 'display', maxValue: dimensionService.getScreenWidth(), maxScrollDistance: 0.7};
  }

  ionViewWillLoad(){
    this.getMyOrderSummary();
    this.getMyOrderList();
  }

  private getMyOrderSummary(){
    this.myOrdersService.getOrderSummary('10611').subscribe(
      result => {
        if(result != null){
          this.model.summary = result;
          console.log(result);
        }
      }
    );
  }

  private getMyOrderList(){
    if(this.model.selected == 'openorder'){
      this.myOrdersService.getOpenOrderList('10611').subscribe(
        result => {
          if(result != null){
            this.model.openOrderList = result;
            this.model.openOrderList.forEach(
              item => {
                item.availability = Math.round(item.availability*10000/item.totalLineItem)/100;
                item.eta = moment(item.eta).format('DD MMMM YYYY');
                item.soDate = moment(item.soDate).format('DD MMM YYYY');
              }
            )
            console.log(result);
          }
        }
      );
    }else{
      this.myOrdersService.getClosedOrderList('10611').subscribe(
        result => {
          if(result != null){
            this.model.closedOrderList = result.data;
            this.model.closedOrderList.forEach(
              item => {
                item.eta = moment(item.eta).format('DD MMMM YYYY');
                item.soDate = moment(item.soDate).format('DD MMM YYYY');
                item.otif = Math.round(item.otif*10000/item.totalLineItem)/100;
              }
            )
            console.log(result);
          }
        }
      );
    }
  }

  public onClickTabs(selected: string){
    this.model.selected = selected;
    this.getMyOrderList();
    this.onClickBackToTop(0);
  }

  public onClickGoBack(){
    this.navCtrl.setRoot(TabsPage, null, {animate: true, animation: 'slide-right'});
  }

  public onClickBackToTop(duration: number){
    console.log(this.content);
    try{
      this.content.scrollTo(0, 0, duration) 
    }catch(error){
      console.log(error);
      this.content.scrollTo(0, 0, duration)
    }
  }

  public onClickCloseOrder(soNumber){
    this.onClickBackToTop(0);
    this.navCtrl.push(MyOrdersClosedOrderPage, { soNumber: soNumber });
    console.log(soNumber);
  }

  public onClickSort(){
    console.log('Sort by clicked');
    const modal = this.modalCtrl.create(MyOrdersSort, null, {cssClass: 'modal-orders-sort', showBackdrop: true});
    modal.present();
  }

  public onClickSendEmail(){
    console.log('Sort by clicked');
    const modal = this.modalCtrl.create(MyOrdersEmail, null, {cssClass: 'modal-orders-email', showBackdrop: true});
    modal.present();
  }

  public onClickOpenOrderDetail(cardSoNumber){
    this.onClickBackToTop(0);
    console.log("Clicked "+cardSoNumber);
    let params = {soNumber: cardSoNumber};
    this.navCtrl.push(MyOrdersOpenOrderPage, params);
  }

  public onClickFilter(){
    console.log('Sort by clicked');
    const modal = this.modalCtrl.create(MyOrdersFilter);
    modal.present();
  }
}
