import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
@Component({
  selector: 'my-orders-email',
  templateUrl: 'my-orders-email.component.html'
})
export class MyOrdersEmail {
  public orderStatus = 'openorder';
  public period = 'monthtodate';

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
  }

  public onClickCloseButton(){
    this.viewCtrl.dismiss();
  }

  public onClickOrderStatus(orderStatus: string){
    this.orderStatus = orderStatus;
  }

  public onClickPeriod(period: string){
    this.period = period;
  }

  public onClickApply(){
    this.viewCtrl.dismiss();
  }

}
