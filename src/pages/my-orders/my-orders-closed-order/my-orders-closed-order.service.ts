import { Injectable } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Observable } from 'rxjs';
import { CONSTANTS } from '../../../common/constants/constants';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyOrdersClosedOrderService{
  constructor(private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }

  public getClosedOrderSummary(customerCode: string, soNumber: string): Observable<any>{
    return this.dataService.get(`https://moqmulatorv2.azurewebsites.net/api/Orders/ClosedOrderDetailSummary?customerCode=${customerCode}&soNumber=${soNumber}`);
  }

  public getClosedOrderList(customerCode: string, soNumber: string): Observable<any>{
    return this.dataService.get(`${CONSTANTS.API.BASE_URL}api/OrdersItem/ClosedOrderItemList?customerCode=${customerCode}&soNumber=${soNumber}`);
  }
}