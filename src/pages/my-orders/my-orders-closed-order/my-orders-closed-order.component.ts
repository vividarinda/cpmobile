import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { MyOrdersClosedOrderModel } from './my-orders-closed-order.model';
import { MyOrdersClosedOrderService } from './my-orders-closed-order.service';
import { LoadingService } from '../../../services/loading.service';
import { DimensionService } from '../../../services/dimension.service';
import { MyOrdersWriteSummaryPage } from './my-orders-write-summary/my-orders-write-summary.component';

@Component({
  selector: 'page-my-orders-closed-order',
  templateUrl: 'my-orders-closed-order.component.html',
  providers: [MyOrdersClosedOrderService]
})
export class MyOrdersClosedOrderPage {

  public model = new MyOrdersClosedOrderModel();
  public viewportWidth: number;

  public rating = [0,1,2,3,4];

  public fabDisplay = false;


  constructor(public navCtrl: NavController, 
              private myOrdersClosedOrderService: MyOrdersClosedOrderService,
              private loadingService: LoadingService,
              public dimensionService: DimensionService,
              private navParams: NavParams) {
    
  }

  ionViewWillLoad(){
    this.getClosedOrderSummary();
    //this.getClosedOrderList();
  }

  private getClosedOrderSummary(){
    this.myOrdersClosedOrderService.getClosedOrderSummary('10611', this.navParams.data.soNumber).subscribe(
      result => {
        if(result != null){
          this.model.summary = result;
          this.model.summary.soDate = moment(this.model.summary.soDate).format('DD MMMM YYYY');
          this.model.summary.lastETA = moment(this.model.summary.lastETA).format('DD MMMM YYYY');
          console.log(result);
        }
      }
    );
  }

  private getClosedOrderList(){
    this.myOrdersClosedOrderService.getClosedOrderList('10611', this.navParams.data.soNumber).subscribe(
      result => {
        if(result != null){
          this.model.partsList = result.data;
          console.log(result);
        }
      }
    );
  }

  public onClickGoBack(){
    this.navCtrl.pop();
  }

  public onClickWriteReview(){
    this.navCtrl.push(MyOrdersWriteSummaryPage);
  }
}
