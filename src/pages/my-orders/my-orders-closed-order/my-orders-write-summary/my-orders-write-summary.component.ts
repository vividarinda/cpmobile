import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment';
import { MyOrdersWriteSummaryService } from './my-orders-write-summary.service';
import { MyOrdersWriteSummaryModel } from './my-orders-write-summary.model';

@Component({
  selector: 'page-my-orders-write-summary',
  templateUrl: 'my-orders-write-summary.component.html',
  providers: [MyOrdersWriteSummaryService]
})
export class MyOrdersWriteSummaryPage {

  public model = new MyOrdersWriteSummaryModel();
  public rating = 0;

  public fabDisplay = false;

  constructor(public navCtrl: NavController, 
              private myOrdersWriteSummaryService: MyOrdersWriteSummaryService,
              private navParams: NavParams) {
    
  }

  ionViewWillLoad(){

  }

  public onClickGoBack(){
    this.navCtrl.pop();
  }

  public onClickRating(rating: number){
    this.rating = rating;
  }
}
