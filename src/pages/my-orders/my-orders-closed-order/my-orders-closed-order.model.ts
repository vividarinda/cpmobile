export class MyOrdersClosedOrderModel{
    summary = new ClosedOrderSummaryModel();
    partsList: PartsModel[] = [];
}

export class ClosedOrderSummaryModel{
    availability: number;
    lastETA: string;
    poCustomer: string;
    reviewDate: string;
    reviewDescription: string;
    reviewStar: number;
    reviewTitle: string;
    soDate: string;
    soNumber: string;
    totalLineItem: number;
}

export class PartsModel{
    partDescription: string;
    partNumber: string;
    quantityAvailable: number;
    quantityOrder: number;
}