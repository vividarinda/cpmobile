export class MyOrdersModel{
  summary: MyOrdersSummary = new MyOrdersSummary();
  openOrderList: OpenOrder[] = [];
  closedOrderList: ClosedOrder[] = [];
  selected = 'openorder';
}

export class MyOrdersSummary{
  totalOrder: number;
  otifPercent: number;
  openOrder: number;
  closedOrder: number;
}

export class OpenOrder{
  poCustomer: string;
  soNumber:	string;
  eta: string =	null;
  soDate:	string;
  totalLineItem: number;
  availability: number;
}

export class ClosedOrder{
  poCustomer: string;
  soNumber: string;
  eta: string;
  soDate: string;
  reviewStar: number;
  totalLineItem: number;
  otif: number;
}