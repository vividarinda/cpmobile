import { Component, ViewChild } from '@angular/core';
import { NavController, Content } from 'ionic-angular';
import { CONSTANTS } from '../../../common/constants/constants';
import { AlertService } from '../../../services/alert.service';
import { LoadingService } from '../../../services/loading.service';
import { MyOrdersOpenOrderService } from './my-orders-open-order.service';
import { MyOrdersOpenOrderModel } from './my-orders-open-order.model';
import { ScrollEventConfig } from '../../../directives/scroll-events/scroll-events';
import { DimensionService } from '../../../services/dimension.service';
import { MyOrdersOpenOrderPartPage } from './my-order-open-order-part/my-orders-open-order-part.component';

@Component({
  selector: 'page-my-orders-open-order',
  templateUrl: 'my-orders-open-order.component.html',
  providers: [MyOrdersOpenOrderService]
})
export class MyOrdersOpenOrderPage {
  @ViewChild(Content) content: Content;
  public model = new MyOrdersOpenOrderModel();
  public displayScrollConfig: ScrollEventConfig = { cssProperty: 'display', maxValue: 1 , maxScrollDistance: 0.7};
  public filterScrollConfig: ScrollEventConfig = { cssProperty: 'display', maxValue: 1 , maxScrollDistance: 0.7, newValue: 'flex'};

  constructor(public navCtrl: NavController,
              private MyEquipmentsStatusUnitDetailService: MyOrdersOpenOrderService,
              private alertService: AlertService,
              private loadingService: LoadingService,
              private dimensionService: DimensionService) {
    this.displayScrollConfig = { cssProperty: 'display', maxValue: dimensionService.getScreenWidth(), maxScrollDistance: 0.7};
    this.filterScrollConfig = { cssProperty: 'display', maxValue: dimensionService.getScreenWidth(), maxScrollDistance: 0.7, newValue: 'flex'};
  }

  ionViewWillLoad(){

  }

  setModel(result){
    if(result != null){
      this.model = result;
    }
  }

  public onClickBackToTop(){
    console.log(this.content);
    try{
      this.content.scrollTo(0, 0, 300) 
    }catch(error){
      console.log(error);
      this.content.scrollTo(0, 0, 300)
    }
  }

  public onClickBack(){
    this.navCtrl.pop();
  }

  public onClickPart(partNumber){
    let soNumber = 'this.model.soNumber';
    let params = {partNumber: partNumber, soNumber: soNumber};
    this.navCtrl.push(MyOrdersOpenOrderPartPage, params);
  }
}
