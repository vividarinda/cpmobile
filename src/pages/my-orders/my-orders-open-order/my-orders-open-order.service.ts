import { Injectable } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { CONSTANTS } from '../../../common/constants/constants';
import { Observable } from 'rxjs';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyOrdersOpenOrderService {

  constructor(private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }

  // public getStatusUnitDetail(unitBrand: string, unitType: string, unitId: number){
  //   return this.dataService.get(CONSTANTS.API.BASE_URL + `myequipment/getstatusunitdetail/${unitBrand}/${unitType}/${unitId}`);
  // }
}