import { Component, ViewChild } from '@angular/core';
import { NavController, Content, ModalController } from 'ionic-angular';
import { CONSTANTS } from '../../../../common/constants/constants';
import { AlertService } from '../../../../services/alert.service';
import { LoadingService } from '../../../../services/loading.service';
import { MyOrdersOpenOrderPartService } from './my-orders-open-order-part.service';
import { MyOrdersOpenOrderPartModel } from './my-orders-open-order-part.model';
import { ScrollEventConfig } from '../../../../directives/scroll-events/scroll-events';
import { DimensionService } from '../../../../services/dimension.service';
import { MyOrdersOpenPartAvailablePage } from './my-orders-open-part-available/my-orders-open-part-available.component';
import { MyOrdersOpenPartOutstandingPage } from './my-orders-open-part-outstanding/my-orders-open-part-outstanding.component';

@Component({
  selector: 'page-my-orders-open-order-part',
  templateUrl: 'my-orders-open-order-part.component.html',
  providers: [MyOrdersOpenOrderPartService]
})
export class MyOrdersOpenOrderPartPage {
  @ViewChild(Content) content: Content;
  public model = new MyOrdersOpenOrderPartModel();
  public displayScrollConfig: ScrollEventConfig = { cssProperty: 'display', maxValue: 1 , maxScrollDistance: 0.8};
  public selected = 'available';

  constructor(public navCtrl: NavController,
              private MyEquipmentsStatusUnitDetailService: MyOrdersOpenOrderPartService,
              private alertService: AlertService,
              public modalCtrl: ModalController,
              private loadingService: LoadingService,
              private dimensionService: DimensionService) {
    this.displayScrollConfig = { cssProperty: 'display', maxValue: dimensionService.getScreenWidth(), maxScrollDistance: 0.8};
  }

  ionViewWillLoad(){

  }

  setModel(result){
    if(result != null){
      this.model = result;
    }
  }

  public onClickBackToTop(){
    console.log(this.content);
    try{
      this.content.scrollTo(0, 0, 300) 
    }catch(error){
      console.log(error);
      this.content.scrollTo(0, 0, 300)
    }
  }

  public onClickBack(){
    this.navCtrl.pop();
  }

  public onClickTabs(type){
    this.selected = type;
  }

  public onClickAvailable(){
    console.log('Available clicked');
    const modal = this.modalCtrl.create(MyOrdersOpenPartAvailablePage, null, {cssClass: 'modal-order-available', showBackdrop: true});
    // modal.onDidDismiss(sorted => {
    //     if(sorted){
    //       console.log(sorted);
    //       this.sorted = sorted;
    //       this.getEquipmentList(this.customerCode,this.plant,this.brandCode,this.type,this.selected,"",1);
    //     }
    //   });
    modal.present();
  }

  public onClickOutstanding(){
    console.log('Outstanding clicked');
    const modal = this.modalCtrl.create(MyOrdersOpenPartOutstandingPage, null, {cssClass: 'modal-order-outstanding', showBackdrop: true});
    // modal.onDidDismiss(sorted => {
    //     if(sorted){
    //       console.log(sorted);
    //       this.sorted = sorted;
    //       this.getEquipmentList(this.customerCode,this.plant,this.brandCode,this.type,this.selected,"",1);
    //     }
    //   });
    modal.present();
  }

}
