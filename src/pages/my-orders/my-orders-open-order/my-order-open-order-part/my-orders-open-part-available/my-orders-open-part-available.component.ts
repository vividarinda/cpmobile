import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
@Component({
  selector: 'my-orders-open-part-available',
  templateUrl: 'my-orders-open-part-available.component.html'
})
export class MyOrdersOpenPartAvailablePage {
  // public sorted = 1;
  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    console.log(navParams);
    // this.sorted = navParams.data.sorted ? navParams.data.sorted : 1;
  }

  public onClickCloseButton(){
    this.viewCtrl.dismiss();
  }

  public onClickSorted(sort){
    // this.sorted = sort;
  }

  public onClickApply(){
    // this.viewCtrl.dismiss(this.sorted);
  }

}
