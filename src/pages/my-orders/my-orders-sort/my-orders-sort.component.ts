import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
@Component({
  selector: 'my-orders-sort',
  templateUrl: 'my-orders-sort.component.html'
})
export class MyOrdersSort {
  public sorted1 = 1;
  public sorted2 = 1;
  public sorted3 = 1;

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    console.log(navParams);
    this.sorted1 = 1;
  }

  public onClickCloseButton(){
    this.viewCtrl.dismiss();
  }

  public onClickSort1(sort: number){
    this.sorted1 = sort;
  }

  public onClickSort2(sort: number){
    this.sorted2 = sort;
  }

  public onClickSort3(sort: number){
    this.sorted3 = sort;
  }

  public onClickApply(){
    this.viewCtrl.dismiss();
  }

}
