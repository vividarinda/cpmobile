import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyOrdersService } from './my-orders.service';
import { MyOrdersPage } from './my-orders.component';
import { DirectivesModule } from '../../directives/directives.module';
import { MyOrdersClosedOrderPage } from './my-orders-closed-order/my-orders-closed-order.component';
import { MyOrdersWriteSummaryPage } from './my-orders-closed-order/my-orders-write-summary/my-orders-write-summary.component';
import { MyOrdersSort } from './my-orders-sort/my-orders-sort.component';
import { MyOrdersEmail } from './my-orders-email/my-orders-email.component';
import { MyOrdersOpenOrderPage } from './my-orders-open-order/my-orders-open-order.component';
import { MyOrdersFilter } from './my-orders-filter/my-orders-filter.component';
import { MyOrdersOpenOrderPartPage } from './my-orders-open-order/my-order-open-order-part/my-orders-open-order-part.component';
import { MyOrdersOpenPartAvailablePage } from './my-orders-open-order/my-order-open-order-part/my-orders-open-part-available/my-orders-open-part-available.component';
import { MyOrdersOpenPartOutstandingPage } from './my-orders-open-order/my-order-open-order-part/my-orders-open-part-outstanding/my-orders-open-part-outstanding.component';

@NgModule({
  declarations: [
    MyOrdersPage,
    MyOrdersOpenOrderPage,
    MyOrdersOpenOrderPartPage,
    MyOrdersOpenPartAvailablePage,
    MyOrdersOpenPartOutstandingPage,
    MyOrdersClosedOrderPage,
    MyOrdersWriteSummaryPage,
    MyOrdersSort,
    MyOrdersEmail,
    MyOrdersOpenOrderPage,
    MyOrdersFilter
  ],
  imports: [
    IonicPageModule.forChild(MyOrdersPage),
    DirectivesModule
  ],
  entryComponents: [
    MyOrdersPage,
    MyOrdersOpenOrderPage,
    MyOrdersOpenOrderPartPage,
    MyOrdersOpenPartAvailablePage,
    MyOrdersOpenPartOutstandingPage,
    MyOrdersClosedOrderPage,
    MyOrdersWriteSummaryPage,
    MyOrdersSort,
    MyOrdersEmail,
    MyOrdersOpenOrderPage,
    MyOrdersFilter
  ],
  providers: [
    MyOrdersService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MyOrdersPageModule {}
