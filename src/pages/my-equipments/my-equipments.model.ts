export class MyEquipmentsModel{
  type: UnitTypeModel[] = [];
  summary: SummaryModel = new SummaryModel();
}

export class SummaryModel{
  totalCount: number;
  availableCount: number;
  notAvailableCount: number;
  operateCount: number;
  notOperateCount: number;
  unitCautionCount: number;
  transmitCount: number;
  lostTransmitCount: number;
}

export class UnitTypeModel{
  type: string;
  amount: number;
  selected = false;
}
