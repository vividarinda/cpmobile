import { Component } from '@angular/core';
import { NavController, ModalController, NavParams, IonicPage, Events } from 'ionic-angular';
import { MyEquipmentsService } from './my-equipments.service';
import { MyEquipmentsModel } from './my-equipments.model';
import { MyEquipmentsModal } from './my-equipments-modal/my-equipments-modal.component';
import { CONSTANTS } from '../../common/constants/constants';
import { LoadingService } from '../../services/loading.service';
import { MyEquipmentsStatusPage } from './my-equipments-status/my-equipments-status.component';

@IonicPage()
@Component({
  selector: 'page-my-equipments',
  templateUrl: 'my-equipments.component.html',
  providers: [MyEquipmentsService]
})
export class MyEquipmentsPage {
  public model = new MyEquipmentsModel();
  public operatePercentage = 0;
  public notOperatePercentage = 0;
  public unitCautionPercentage = 0;
  public availablePercentage = 0;
  public notAvailablePercentage = 0;
  public transmitPercentage = 0;
  public lostTransmitPercentage = 0;
  public selected = 'ALL';
  public paramBrandCode = null;
  public type = '';
  public image = CONSTANTS.LOGO;
  
  constructor(public navCtrl: NavController,
              private myEquipmentsService: MyEquipmentsService,
              private modalCtrl: ModalController,
              private loadingService: LoadingService,
              private navParams: NavParams,
              private events: Events) {
    
    this.selected = this.navParams.data.params.brand == "" ? "ALL" : this.navParams.data.params.brand;
    this.paramBrandCode = this.navParams.data.params.brandCode;

    this.events.subscribe('change-tab', (tab, params) => {
      this.selected = params.brand == "" ? "ALL" : params.brand;
      this.paramBrandCode = params.brandCode == null ? params.brand : params.brandCode;
      this.ionViewWillLoad();
      this.navCtrl.goToRoot({});
    });
  }

  ionViewWillLoad(){
    
    this.getEquipmentsByType("10611", "",this.paramBrandCode,this.type);
    this.getAllEquipmentsHeader("10611","",this.paramBrandCode);
  }

  getAllEquipmentsHeader(customerCode:string,plant:string,brandCode:string){
    this.myEquipmentsService.getEquipmentsHeaders(customerCode,plant,brandCode).subscribe(result =>{
      this.loadingService.dismissLoading();
      if(result != null){
        // this.setModel(result);
        this.model.type = result;
        this.model.type[0].selected = true;
      }
    },
      error => {
        this.loadingService.dismissLoading();
      }
    )
  }

  getEquipmentsByType(customerCode: string, plant: string, brandCode: string, type: string){
    this.myEquipmentsService.getEquipmentsByType(customerCode, plant, brandCode, type).subscribe(result =>{
      this.loadingService.dismissLoading();
      if(result != null){
        this.setModel(result);
      }
    },
      error => {
        this.loadingService.dismissLoading();
      }
    );
  }

  onCLickEquipmentsType(unitType){
    this.type = unitType;
    console.log(this.type);
    this.model.type.forEach(type => type.selected = type.type == unitType ? true : false);
    this.getEquipmentsByType("10611","",this.paramBrandCode,unitType);
  }

  setModel(result){
    if(result != null){
      this.model.summary = result;
      this.operatePercentage =  this.calculatePercentage(this.model.summary.operateCount, this.model.summary.availableCount);
      this.notOperatePercentage = this.calculatePercentage(this.model.summary.notOperateCount, this.model.summary.availableCount);
      this.availablePercentage = this.calculatePercentage(this.model.summary.availableCount, this.model.summary.totalCount);
      this.unitCautionPercentage = this.calculatePercentage(this.model.summary.unitCautionCount, this.model.summary.availableCount);
      this.notAvailablePercentage = this.calculatePercentage(this.model.summary.notAvailableCount, this.model.summary.totalCount);
      this.transmitPercentage = this.calculatePercentage(this.model.summary.transmitCount, this.model.summary.availableCount);
      this.lostTransmitPercentage = this.calculatePercentage(this.model.summary.lostTransmitCount, this.model.summary.availableCount);
    }
  }

  calculatePercentage(count: number, totalCount: number){
    return totalCount != 0 ? count * 100 / totalCount : 0;
  }

  public onClickBack(){
    //this.navCtrl.pop({animate: true, animation: "transition-ios"});
    this.navCtrl.parent.select(0);
  }

  public onClickEquipmentsDropdown(){
    let myEquipmentsModal = this.modalCtrl.create(MyEquipmentsModal, {brand: this.selected}, {cssClass: 'modal-equipments', showBackdrop: true});
    myEquipmentsModal.onDidDismiss(selected => {
      if(selected){
        console.log(selected);
        this.selected = selected.brand == "" ? "ALL" : selected.brand;
        this.paramBrandCode = selected.brandCode == null ? selected.brand : selected.brandCode;
        this.getEquipmentsByType("10611", "" ,this.paramBrandCode, "");
        this.getAllEquipmentsHeader("10611", "", this.paramBrandCode);
      }
    });
    myEquipmentsModal.present();
  }

  public onClickMyEquipmentsStatusPage(statusType: string){
    let params = {status: statusType, brand: this.selected, brandCode: this.paramBrandCode, type: this.type};
    console.log(params);
    this.navCtrl.push(MyEquipmentsStatusPage, params);
  }
}