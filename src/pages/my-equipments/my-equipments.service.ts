import { Injectable } from '@angular/core';
import { DataService } from '../../services/data.service';
import { CONSTANTS } from '../../common/constants/constants';
import { Observable } from 'rxjs';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyEquipmentsService {

  constructor(private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }

  public getAllEquipments(customerCode: string): Observable<any>{
    return this.dataService.get(`${CONSTANTS.API.BASE_URL}api/equipment/summary?customerCode=${customerCode}`);
  }

  public getEquipmentsByType(customerCode: string, plant: string, brandCode: string, type: string){
    return this.dataService.get(`${CONSTANTS.API.BASE_URL}api/equipment/summary?customerCode=${customerCode}&plant=${plant}&brandCode=${brandCode}&type=${type}`);
  }

  public getEquipmentsHeaders(customerCode: string, plant: string, brandCode: string){
    return this.dataService.get(`${CONSTANTS.API.BASE_URL}api/equipment/summary/header?customerCode=${customerCode}&plant=${plant}&brandCode=${brandCode}`);
  }
}