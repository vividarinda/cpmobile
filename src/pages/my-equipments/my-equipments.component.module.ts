import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimelineModule } from '../../components/timeline/timeline.module';
import { MyEquipmentsPage } from './my-equipments.component';
import { MyEquipmentsModal } from './my-equipments-modal/my-equipments-modal.component';
import { MyEquipmentsStatusModal } from './my-equipments-status/my-equipments-status-modal/my-equipments-status-modal.component';
import { DirectivesModule } from '../../directives/directives.module';
import { MyEquipmentsStatusPage } from './my-equipments-status/my-equipments-status.component';
import { MyEquipmentsStatusUnitDetailPage } from './my-equipments-status/my-equipments-status-unit/my-equipments-status-unit-detail/my-equipments-status-unit-detail.component';
import { MyEquipmentsStatusUnitPage } from './my-equipments-status/my-equipments-status-unit/my-equipments-status-unit.component';
import { MyEquipmentsLocationPage } from './my-equipments-status/my-equipments-status-unit/my-equipments-location/my-equipments-location.component';
import { MyEquipmentsStatusUnitChartPage } from './my-equipments-status/my-equipments-status-unit/my-equipments-status-unit-chart/my-equipments-status-unit-chart.component';
import { MyEquipmentsStatusSort } from './my-equipments-status/my-equipments-status-sort/my-equipments-status-sort.component';
import { MyEquipmentsService } from './my-equipments.service';

@NgModule({
  declarations: [
    MyEquipmentsPage,
    MyEquipmentsModal,
    MyEquipmentsStatusModal,
    MyEquipmentsStatusSort,
    MyEquipmentsStatusPage,
    MyEquipmentsStatusUnitPage,
    MyEquipmentsStatusUnitDetailPage,
    MyEquipmentsLocationPage,
    MyEquipmentsStatusUnitChartPage
  ],
  imports: [
    IonicPageModule.forChild(MyEquipmentsPage),
    TimelineModule,
    DirectivesModule
  ],
  entryComponents: [
    MyEquipmentsPage,
    MyEquipmentsModal,
    MyEquipmentsStatusModal,
    MyEquipmentsStatusSort,
    MyEquipmentsStatusPage,
    MyEquipmentsStatusUnitPage,
    MyEquipmentsStatusUnitDetailPage,
    MyEquipmentsLocationPage,
    MyEquipmentsStatusUnitChartPage
  ],
  providers: [
    MyEquipmentsService
  ]
})
export class MyEquipmentsPageModule {}
