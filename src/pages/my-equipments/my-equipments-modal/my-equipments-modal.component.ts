import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { MyEquipmentsModalService } from './my-equipments-modal.service';
import { MyEquipmentsModalModel } from './my-equipments-modal.model';
@Component({
  selector: 'my-equipments-modal',
  templateUrl: 'my-equipments-modal.component.html',
  providers: [MyEquipmentsModalService]
})
export class MyEquipmentsModal {
  public activeMenu = null;
  public model = new MyEquipmentsModalModel();

  constructor(public myEquipmentsModalService: MyEquipmentsModalService,public viewCtrl: ViewController, private navParams: NavParams) {
    this.activeMenu = navParams.data.brand == "" ? "ALL" : navParams.data.brand;
    console.log(navParams.data.brand);
  }

  ionViewWillLoad(){
    this.getEquipmentData("10611");
  }

  ionViewWillLeave(){
    
  }

  getEquipmentData(customerCode: string){
    this.myEquipmentsModalService.getEquipmentData(customerCode).subscribe(
      result => { if(result != null) {
          this.model.equipments = result;
        }
      }
    );
  }

  private getByBrand(brandName: string){
    let amount = 0;
    if(this.model.equipments.find(item => item.brandName == brandName)){
      amount = this.model.equipments.find(item => item.brandName == brandName).amount;
    }
    return amount;
  }

  public onClickCloseButton(){
    this.viewCtrl.dismiss();
  }

  public onClickMyEquipmentsCard(clicked: string){
    this.activeMenu = clicked;
    this.viewCtrl.dismiss(clicked);
  }

  public onClickEquipment(brand: string){
    brand = brand == "ALL" ? "" : brand;
    let brandCode = null;
    if(this.model.equipments.find(item => item.brandName == brand)){
      brandCode = this.model.equipments.find(item => item.brandName == brand).brandCode;
    }
    this.activeMenu = brand;
    this.viewCtrl.dismiss({brand:brand,brandCode:brandCode});
  }
}
