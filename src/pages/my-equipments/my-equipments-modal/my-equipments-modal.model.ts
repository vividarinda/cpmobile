export class MyEquipmentsModalModel{
  equipments: EquipmentsModel[] = [];
}

export class EquipmentsModel{
  brandCode: string;
  brandName: string;
  brandImagePath: string;
  amount: number;
  order: number;
}