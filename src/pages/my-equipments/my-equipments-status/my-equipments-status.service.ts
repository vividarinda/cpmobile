import { Injectable } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { CONSTANTS } from '../../../common/constants/constants';
import { Observable } from 'rxjs';

@Injectable()
export class MyEquipmentsStatusService {

  constructor(private dataService: DataService) {
    console.log('MyEquipmentStatus Service called');
  }

  public getEquipmentHeader(customerCode,plant,brandCode,type): Observable<any>{
    return this.dataService.get(CONSTANTS.API.BASE_URL + 'api/equipment/header?customerCode=' + customerCode + '&plant=' + plant + '&brandCode=' + brandCode + '&type=' + type);
  }

  public getAllEquipmentsByStatus(customerCode,plant,brandCode,type,status,keyword,page,sorting): Observable<any>{
    return this.dataService.get(CONSTANTS.API.BASE_URL + 'api/equipment?customerCode=' + customerCode + '&plant=' + plant + '&brandCode=' + brandCode + '&type=' + type + '&status=' + status + '&keyword=' + keyword + '&page=' + page + '&sorting=' + sorting);
  }
}