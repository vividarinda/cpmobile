import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
@Component({
  selector: 'my-equipments-status-modal',
  templateUrl: 'my-equipments-status-modal.component.html'
})
export class MyEquipmentsStatusModal {
  public selected = null;
  public selectedTitle = null;
  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.selected = navParams.data.selected.toUpperCase();
    this.selectedTitle = this.getTitle();
    console.log(this.selected);
  }

  public onClickCloseButton(){
    this.viewCtrl.dismiss();
  }

  public getTitle(){
    return this.selected.toLowerCase()
    .split(' ')
    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    .join(' ');
  }
}
