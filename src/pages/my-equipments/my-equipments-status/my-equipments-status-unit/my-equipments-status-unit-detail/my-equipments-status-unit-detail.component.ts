import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyEquipmentsStatusUnitDetailService } from './my-equipments-status-unit-detail.service';
import { MyEquipmentsStatusUnitDetailModel } from './my-equipments-status-unit-detail.model';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { CONSTANTS } from '../../../../../common/constants/constants';
import { AlertService } from '../../../../../services/alert.service';
import { LoadingService } from '../../../../../services/loading.service';

@Component({
  selector: 'page-my-equipments-status-unit-detail',
  templateUrl: 'my-equipments-status-unit-detail.component.html',
  providers: [MyEquipmentsStatusUnitDetailService]
})
export class MyEquipmentsStatusUnitDetailPage {
  public model = new MyEquipmentsStatusUnitDetailModel();
  public equipmentId = null;
  
  constructor(public navCtrl: NavController,
              private MyEquipmentsStatusUnitDetailService: MyEquipmentsStatusUnitDetailService,
              private nativePageTransitions: NativePageTransitions,
              private alertService: AlertService,
              private loadingService: LoadingService,
              public navParams: NavParams) {
    this.model.brandName = navParams.data.brandName;
    this.equipmentId = navParams.data.equipmentId;
  }

  ionViewWillLoad(){
    this.getStatusUnitDetail(this.equipmentId);
  }

  getStatusUnitDetail(equipmentId: string){
    this.MyEquipmentsStatusUnitDetailService.getStatusUnitDetail(equipmentId).timeout(CONSTANTS.API.TIME_OUT_VAL).subscribe(result =>{
      this.loadingService.dismissLoading();
      if(result != null){
        this.setModel(result);
        console.log(result);
      }
    },
      error => {
        this.alertService.showError(error.message)
        this.loadingService.dismissLoading();
      }
    );
  }

  setModel(result){
    if(result != null){
      this.model.cautions = result;
    }
  }

  ionViewWillLeave(){
  }

  public onClickBack(){
    this.navCtrl.pop();
  }

}
