import { Injectable } from '@angular/core';
import { DataService } from '../../../../../services/data.service';
import { CONSTANTS } from '../../../../../common/constants/constants';
import { Observable } from 'rxjs';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyEquipmentsStatusUnitDetailService {

  constructor(private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }

  public getStatusUnitDetail(equipmentId: string){
    return this.dataService.get(CONSTANTS.API.BASE_URL + `api/equipment/${equipmentId}/caution`);
  }
}