export class MyEquipmentsStatusUnitDetailModel{
    brandName: string;
    cautions: CautionModel[] = [];
}

export class CautionModel{
    cautionId: string;
    description: string;
    cautionSteps: cautionStepModel[] = [];
}

export class cautionStepModel{
    cautionStepId: string;
    order: number;
    description: string;
}