import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyEquipmentsStatusUnitChartService } from './my-equipments-status-unit-chart.service';
import { MyEquipmentsStatusUnitChartModel } from './my-equipments-status-unit-chart.model';
import { CONSTANTS } from '../../../../../common/constants/constants';
import { AlertService } from '../../../../../services/alert.service';
import { LoadingService } from '../../../../../services/loading.service';

@Component({
  selector: 'page-my-equipments-status-unit-chart',
  templateUrl: 'my-equipments-status-unit-chart.component.html',
  providers: [MyEquipmentsStatusUnitChartService]
})
export class MyEquipmentsStatusUnitChartPage {
  public model = new MyEquipmentsStatusUnitChartModel();
  public hours = [24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0];
  public color = ["#FFD500", "#E97E5D", "#FF5E73", "#27AE60", "#3274EE", "#ef9a9a", "#f48fb1", "#ce93d8", "#b39ddb", "#9fa8da", "#90caf9", "#81d4fa", "#80deea", "#80cbc4", "#a5d6a7", "#c5e1a5", "#e6ee9c", "#fff59d", "#ffe082", "#ffcc80", "#ffab91", "#bcaaa4", "#eeeeee", "#b0bec5", "#f44336", "#e91e63", "#6a1b9a", "#673ab7", "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4", "#009688", "#4caf50", "#8bc34a", "#cddc39", "#ffeb3b", "#ffc107", "#ff9800", "#ff5722"];
  public equipmentId = null;
  
  constructor(public navCtrl: NavController,
              private MyEquipmentsStatusUnitChartService: MyEquipmentsStatusUnitChartService,
              private alertService: AlertService,
              private navParams: NavParams,
              private loadingService: LoadingService) {
    console.log(navParams);
    this.equipmentId = navParams.data.equipmentId;
  }

  ionViewWillLoad(){
    this.getUnitDetail(this.equipmentId);
  }

  getUnitDetail(equipmentId: string){
    this.MyEquipmentsStatusUnitChartService.getUnitDetail(equipmentId).timeout(CONSTANTS.API.TIME_OUT_VAL).subscribe(result =>{
      // this.loadingService.dismissLoading();
      if(result != null){
        this.setModel(result);
        console.log(this.getWorkingCharateristicObject());
      }
    },
      error => {
        this.alertService.showError(error.message)
        this.loadingService.dismissLoading();
      }
    );
  }

  setModel(result){
    if(result != null){
      this.model = result;
    }
  }
 
  public getWorkingCharateristicObject(){
    let workingCharateristicObject;
    for (let groupIndex = 0; groupIndex < this.model.characteristicGroups.length; groupIndex++) {
      const groupItem = this.model.characteristicGroups[groupIndex];
      if(groupItem.type == "WorkingCharacteristic"){
        workingCharateristicObject = groupItem.characteristics;
      }
    }
    return workingCharateristicObject;
  }

  public getWorkingCharateristicLength(){
    let workingCharateristicLength;
    for (let groupIndex = 0; groupIndex < this.model.characteristicGroups.length; groupIndex++) {
      const groupItem = this.model.characteristicGroups[groupIndex];
      if(groupItem.type == "WorkingCharacteristic"){
        workingCharateristicLength = groupItem.characteristics.length;
      }
    }
    return workingCharateristicLength;
  }

  public onClickBack(){
    this.navCtrl.pop();
  }

  public getColor(index){
    return this.color[index % 40];
  }

  public getBarWidth(){
    // console.log((this.model.workingCharacteristic.length * 18.6) + 5)+"vw";
    let objectLength = this.getWorkingCharateristicLength();
    return ((objectLength * 18.6) + 5)+"vw";
  }
  
  public getScaleWidth(){
    // console.log((this.model.workingCharacteristic.length * 18.6) + 20)+"vw";
    let objectLength = this.getWorkingCharateristicLength();
    return ((objectLength * 18.6) + 20)+"vw";
  }

  public getHeight(value){
    // console.log((value/24*120)+"vw");
    return (value/24*120)+"vw";
  }

  public getMarginTop(value){
    // console.log(((24-value)/24*120)+"vw");
    return ((24-value)/24*120)+"vw";
  }

}
