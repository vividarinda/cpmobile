import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform, NavParams } from 'ionic-angular';
import { MyEquipmentsLocationService } from './my-equipments-location.service';
import { MyEquipmentsLocationModel } from './my-equipments-location.model';
import {
  GoogleMaps,
  GoogleMap,
  LatLng,
  GoogleMapsEvent,
} from '@ionic-native/google-maps';

@Component({
  selector: 'page-my-equipments-location',
  templateUrl: 'my-equipments-location.component.html',
  providers: [MyEquipmentsLocationService]
})
export class MyEquipmentsLocationPage {
  @ViewChild('map') mapElement: ElementRef;
  private map:GoogleMap;
  private location:LatLng;
  public model = new MyEquipmentsLocationModel();
  private brandName:string;
  private modelNumber:string;
  private serialNumber:string;

  constructor(private platform: Platform,
    private googleMaps: GoogleMaps, private navController: NavController, private navParams: NavParams) {
      // this.location = new LatLng(42.346903, -71.135101);
      this.location = new LatLng(navParams.data.lat,navParams.data.long);
      this.brandName = navParams.data.brandName;
      this.modelNumber = navParams.data.modelNumber;
      this.serialNumber = navParams.data.serialNumber;
      this.platform.registerBackButtonAction(()=>{
        this.navController.pop({animate:false});
      });
  }

  ionViewDidLoad() {
    this.loadMap();
  }

  ionViewWillLeave(){
    this.platform.registerBackButtonAction(()=>{
      this.navController.pop({animate:true});
    });
  }

  loadMap(){
    this.platform.ready().then(() => {
      let element = this.mapElement.nativeElement;
      this.map = this.googleMaps.create(element);
   
      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        let options = {
          target: this.location,
          zoom: 18
        };
   
        this.map.moveCamera(options);
        this.map.addMarker({position: this.location});
      });
    });
  }

  onClickBack(){
    this.navController.pop({animate:false});
  }
}
