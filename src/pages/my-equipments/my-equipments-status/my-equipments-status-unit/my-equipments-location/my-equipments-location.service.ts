import { Injectable } from '@angular/core';
import { DataService } from '../../../../../services/data.service';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyEquipmentsLocationService {

  constructor(private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }
}