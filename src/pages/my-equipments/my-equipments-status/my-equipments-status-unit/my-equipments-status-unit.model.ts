export class MyEquipmentsStatusUnitModel{
    equipmentId: string;
    brandId: string;
    brandName: string;
    serialNumber: string;
    modelNumber: string;
    type: string;
    unitCode: string;
    plantDescription: string;
    transmittedPlantKabupaten: string;
    transmittedPlantZipCode: string;
    isUnitCaution: boolean;
    warrantyEndDate: string;
    warrantyStatus: number;
    warrantyStatusString: string;
    operationStatus: number;
    operationStatusString: string;
    smrTotalInMinutes: number
    smrLastValueDate: string;
    lastTransmitted: string;
    imagePath: string;
    numberOfCaution: number;
    isTransmitted: boolean;
    transmittedLatitude: number;
    transmittedLongitude: number;
    characteristicGroups : characteristicGroupsModel[] = [];
    mailTo: mailToModel;
}

export class mailToModel{
    destinationAddresses: string[] = [];
    ccAddresses: string[] = [];
    senderAddress: string;
    subject: string;
    content: string;
}

export class characteristicGroupsModel{
    type: string;
    characteristics: characteristicsModel[] = [];
}

export class characteristicsModel{
    equipmentValueId: string;
    name: string;
    type: string;
    value: number;
}