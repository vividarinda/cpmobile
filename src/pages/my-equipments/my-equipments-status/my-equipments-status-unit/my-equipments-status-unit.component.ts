import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyEquipmentsStatusUnitService } from './my-equipments-status-unit.service';
import { MyEquipmentsStatusUnitModel } from './my-equipments-status-unit.model';
import { CONSTANTS } from '../../../../common/constants/constants';
import { AlertService } from '../../../../services/alert.service';
import { LoadingService } from '../../../../services/loading.service';
import { MyEquipmentsStatusUnitDetailPage } from './my-equipments-status-unit-detail/my-equipments-status-unit-detail.component';
import { MyEquipmentsStatusUnitChartPage } from './my-equipments-status-unit-chart/my-equipments-status-unit-chart.component';
import { MyEquipmentsLocationPage } from './my-equipments-location/my-equipments-location.component';
import * as moment from 'moment';

@Component({
  selector: 'page-my-equipments-status-unit',
  templateUrl: 'my-equipments-status-unit.component.html',
  providers: [MyEquipmentsStatusUnitService]
})
export class MyEquipmentsStatusUnitPage {
  public model = new MyEquipmentsStatusUnitModel();
  public show = false;
  public equipmentId = null;
  public color = ["#FFD500", "#E97E5D", "#FF5E73", "#27AE60", "#3274EE"];
  
  constructor(public navCtrl: NavController,
              private MyEquipmentsStatusUnitService: MyEquipmentsStatusUnitService,
              private alertService: AlertService,
              private navParams: NavParams,
              private loadingService: LoadingService) {
    console.log(navParams);
    this.equipmentId = navParams.data.equipmentId;
  }

  ionViewWillLoad(){
    // console.log(this.show);
    this.getUnitDetail(this.equipmentId);
  }

  ionViewWillLeave(){
    // console.log(this.show);
  }

  getUnitDetail(equipmentId: string){
    this.MyEquipmentsStatusUnitService.getUnitDetail(equipmentId).timeout(CONSTANTS.API.TIME_OUT_VAL).subscribe(result =>{
      // this.loadingService.dismissLoading();
      if(result != null){
        // console.log(result);
        this.setModel(result);
        console.log(this.model);
        this.show = true;
        if(this.model.operationStatus == 2){
          this.getMailToData(equipmentId);
        }
      }
    },
      error => {
        this.alertService.showError(error.message)
        this.loadingService.dismissLoading();
      }
    );
  }

  getMailToData(equipmentId: string){
    this.MyEquipmentsStatusUnitService.getMailToData(equipmentId).timeout(CONSTANTS.API.TIME_OUT_VAL).subscribe(result =>{
      // this.loadingService.dismissLoading();
      if(result != null){
        this.model.mailTo =  result;
      }
    },
      error => {
        this.alertService.showError(error.message)
        this.loadingService.dismissLoading();
      }
    );
  }

  setModel(result){
    if(result != null){
      this.model = result;
    }
  }

  public getFuelObject(){
    let fuelObject;
    for (let groupIndex = 0; groupIndex < this.model.characteristicGroups.length; groupIndex++) {
      const groupItem = this.model.characteristicGroups[groupIndex];
      if(groupItem.type == "Fuel"){
        for (let charIndex = 0; charIndex < groupItem.characteristics.length; charIndex++) {
          const charItem = groupItem.characteristics[charIndex];
          if(charItem.type == "Fuel"){
            fuelObject = charItem;
          }
        }
      }
    }
    return fuelObject;
  }
  
  public getTotalWorkingHoursObject(){
    let totalWorkingHoursObject;
    for (let groupIndex = 0; groupIndex < this.model.characteristicGroups.length; groupIndex++) {
      const groupItem = this.model.characteristicGroups[groupIndex];
      if(groupItem.type == "WorkingHoursPerDay"){
        for (let charIndex = 0; charIndex < groupItem.characteristics.length; charIndex++) {
          const charItem = groupItem.characteristics[charIndex];
          if(charItem.type == "WorkingHours"){
            totalWorkingHoursObject = charItem;
          }
        }
      }
    }
    return totalWorkingHoursObject;
  }
  
  public getActualWorkingHoursObject(){
    let actualWorkingHoursObject;
    for (let groupIndex = 0; groupIndex < this.model.characteristicGroups.length; groupIndex++) {
      const groupItem = this.model.characteristicGroups[groupIndex];
      if(groupItem.type == "WorkingHoursPerDay"){
        for (let charIndex = 0; charIndex < groupItem.characteristics.length; charIndex++) {
          const charItem = groupItem.characteristics[charIndex];
          if(charItem.type == "ActualWorkingHours"){
            actualWorkingHoursObject = charItem;
          }
        }
      }
    }
    return actualWorkingHoursObject;
  }
  
  public getWorkingCharateristicObject(){
    let workingCharateristicObject;
    for (let groupIndex = 0; groupIndex < this.model.characteristicGroups.length; groupIndex++) {
      const groupItem = this.model.characteristicGroups[groupIndex];
      if(groupItem.type == "WorkingCharacteristic"){
        workingCharateristicObject = groupItem.characteristics;
      }
    }
    return workingCharateristicObject;
  }

  public getWarrantyEndDate(date):string{
    return moment(date).format('DD MMMM YYYY');
  }

  public getSMRDate(date):string{
    return moment(date).format('DD-MM-YY');
  }

  public getTransmittedDate(date):string{
    return moment(date).format('DD MMMM YYYY');
  }

  public getTransmittedTime(date):string{
    return moment(date).format('HH:mm:ss');
  }

  public getHeight(value){
    // console.log((value/24*13.5)+"vw");
    return (value/24*13.5)+"vw";
  }

  public getMarginTop(value){
    // console.log(((24-value)/24*13.5)+"vw");
    return ((24-value)/24*13.5)+"vw";
  }

  public onClickBack(){
    this.navCtrl.pop();
  }

  public onClickStatusUnitDetail(){
    let params = { brandName : this.model.brandName, equipmentId : this.model.equipmentId};
    this.navCtrl.push(MyEquipmentsStatusUnitDetailPage, params);
  }

  public onClickViewDetail(){
    let params = {equipmentId: this.model.equipmentId};
    this.navCtrl.push(MyEquipmentsStatusUnitChartPage, params);
  }
  public onClickLocation(){
    let params = { lat : this.model.transmittedLatitude, long : this.model.transmittedLongitude, brandName : this.model.brandName, modelNumber: this.model.modelNumber, serialNumber: this.model.serialNumber};
    this.navCtrl.push(MyEquipmentsLocationPage, params, {
      animate : false
    });
  }

  public onClickRequestChange(){
    let emailAddress = this.model.mailTo.destinationAddresses.toString();
    let ccAddress = this.model.mailTo.ccAddresses.toString();
    let emailSubject = this.model.mailTo.subject;
    let emailBody = this.model.mailTo.content;
    console.log(emailBody);
    // let emailBody = "Dear UT Call, \n\n Mohon untuk pergantian status unit di bawah ini: \n Nama Customer : BUKIT MAKMUR MANDIRI UTAMA \n Model Number : D85E-SS-2 \n Serial Number : J18157 \n Status Baru : Unit sudah tidak dioperasikan lagi \n Lokasi : SBY \n\n Terima Kasih";
    window.open(encodeURI('mailto:'+emailAddress+'?cc='+ccAddress+'&subject='+emailSubject+'&body='+emailBody));
  }
}
