import { Component } from '@angular/core';
import { NavController, ModalController, NavParams, Events } from 'ionic-angular';
import { MyEquipmentsStatusModel } from './my-equipments-status.model';
import { MyEquipmentsStatusModal } from './my-equipments-status-modal/my-equipments-status-modal.component';
import { LoadingService } from '../../../services/loading.service';
import { MyEquipmentsStatusUnitPage } from './my-equipments-status-unit/my-equipments-status-unit.component';
import { MyEquipmentsStatusService } from './my-equipments-status.service';
import { MyEquipmentsStatusSort } from './my-equipments-status-sort/my-equipments-status-sort.component';
import * as moment from 'moment';


@Component({
    selector: 'page-my-equipments-status',
    templateUrl: 'my-equipments-status.component.html',
    providers: [MyEquipmentsStatusService]
})
export class MyEquipmentsStatusPage {
    public model = new MyEquipmentsStatusModel();
    public warranty = false;
    public selected = '';
    public brand = 'ALL';
    public type = '';
    public brandCode = null;
    public customerCode = "";
    public plant = "";
    public showMoreLoading = false;
    public sorted = 0;
    public searchInput = '';

    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController,
        private myEquipmentsStatusService: MyEquipmentsStatusService,
        private navParams: NavParams,
        private loadingService: LoadingService,
        private events: Events) {
        console.log(navParams);
        this.selected = navParams.data.status;
        this.brand = navParams.data.brand;
        this.brandCode = navParams.data.brandCode;
        this.type = navParams.data.type;
        this.customerCode = "10611";
        this.plant = "";
    }

    ionViewWillLoad(){
        this.getEquipmentList(this.customerCode,this.plant,this.brandCode,this.type,this.selected,"",1);
        this.getEquipmentListHeader(this.customerCode,this.plant,this.brandCode,this.type);
    }

    public onClickGoBack(){
        this.navCtrl.pop();
    }

    public onClickGetInformation(){
        console.log('Information clicked');
        const modal = this.modalCtrl.create(MyEquipmentsStatusModal, 
            {selected: this.selected}, {cssClass: 'modal-equipment-status', showBackdrop: true});
        modal.present();
    }

    public onClickUnitDetail(equipmentId){
        let params = {equipmentId: equipmentId};
        this.navCtrl.push(MyEquipmentsStatusUnitPage, params);
    }

    public getEquipmentList(customerCode,plant,brandCode,type,status,keyword,page){
        let paramStatus = status.replace(/\s/g, '_').toLowerCase();
        this.myEquipmentsStatusService.getAllEquipmentsByStatus(customerCode,plant,brandCode,type, paramStatus,keyword,page,this.sorted).subscribe((result) => {
            this.loadingService.dismissLoading();
            if(result != null){
                this.model.list = result.data;
                this.setDataAttribute(result);
                console.log(this.model);
            }
        }, (error) => {
            this.loadingService.dismissLoading();
        })
    }

    public setDataAttribute(result){
        this.model.pageIndex = result.pageIndex;
        this.model.pageSize = result.pageSize;
        this.model.totalCount = result.totalCount;
        this.model.totalPageCount = result.totalPageCount;
        this.model.startingNumber = result.startingNumber;
        this.model.hasPreviousPage = result.hasPreviousPage;
        this.model.hasNextPage = result.hasNextPage;
    }

    public getEquipmentListHeader(customerCode,plant,brandCode,type){
        this.myEquipmentsStatusService.getEquipmentHeader(customerCode,plant,brandCode,type).subscribe((result) => {
            if(result != null){
                this.model.header = result;
                this.model.header.forEach((item) => {
                    if(item.status.toUpperCase() == this.selected.toUpperCase()){
                        item.selected = true;
                    }
                });
                console.log(this.model.header);
            }
        }, (error) => {
           console.log(error); 
        });
    }

    public onClickStatus(status: string){
        this.selected = status;
        console.log(status);
        this.model.header.forEach((item) => {
            item.selected = item.status == status ? true : false;
        });
        this.getEquipmentList(this.customerCode,this.plant,this.brandCode,this.type,status,"",1);
    }

    public getSMRDate(date):string{
        if(date){
            return moment(date).format('DD-MM-YY');
        }else{
            return "";
        }
    }

    public getMoreData(){
        console.log("Show More");
        this.showMoreLoading = true;
        let paramStatus = this.selected.replace(/\s/g, '_').toLowerCase();
        this.myEquipmentsStatusService.getAllEquipmentsByStatus(this.customerCode,this.plant,this.brandCode,this.type, paramStatus,this.searchInput,this.model.pageIndex+1,this.sorted).subscribe((result) => {
            this.loadingService.dismissLoading();
            if(result != null){
                this.showMoreLoading = false;
                this.model.list = [...this.model.list, ...result.data];
                this.setDataAttribute(result);
                console.log(this.model);
            }
        }, (error) => {
            this.loadingService.dismissLoading();
        })
    }

    public onClickSortBy(){
        console.log('Sort by clicked');
        let params = {sorted: this.sorted};
        const modal = this.modalCtrl.create(MyEquipmentsStatusSort, params, {cssClass: 'modal-equipment-sort', showBackdrop: true});
        modal.onDidDismiss(sorted => {
            if(sorted){
              console.log(sorted);
              this.sorted = sorted;
              this.getEquipmentList(this.customerCode,this.plant,this.brandCode,this.type,this.selected,"",1);
            }
          });
        modal.present();
    }

    public onSearchSubmit(){
        console.log('submited');
        this.getEquipmentList(this.customerCode,this.plant,this.brandCode,this.type,this.selected,this.searchInput,1);
    }

    public onClickReset(){
        console.log('reseted');
        this.searchInput='';
        this.getEquipmentList(this.customerCode,this.plant,this.brandCode,this.type,this.selected,'',1);
    }
}