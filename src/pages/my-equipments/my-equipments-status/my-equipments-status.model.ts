export class MyEquipmentsStatusModel {
    header: HeaderModel[] = [];
    list: UnitStatusModel[] = [];
    pageIndex: number;
    pageSize: number;
    totalCount: number;
    totalPageCount: number;
    startingNumber: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
}

export class HeaderModel{
    status: string;
    amount: number;
    selected = false;
}

export class UnitStatusModel{
    equipmentId: string;
    fuelConsumptionPerDay: number;
    imagePath: string;
    isUnitCaution: boolean;
    modelNumber: string;
    operationStatus: number;
    operationStatusString: string;
    serialNumber: string;
    smrLastValueDate: string;
    smrTotalInMinutes: number;
    smrValuePerDayInMinutes: number;
    warrantyStatus: number;
    warrantyStatusString: string;
}