import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home.component';
import { HomeService } from './home.service';
import { TimelineModule } from '../../components/timeline/timeline.module';
import { TicketSubmissionFormPage } from './ticket-submission-form/ticket-submission-form.component';
import { DirectivesModule } from '../../directives/directives.module';
import { SearchModal } from './search/search.component';
import { SearchResultPage } from './search/search-result/search-result.component';
import { LocationFilterModal } from './location-filter/location-filter.component';

@NgModule({
  declarations: [
    HomePage,
    SearchModal,
    SearchResultPage,
    TicketSubmissionFormPage,
    LocationFilterModal
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    TimelineModule,
    DirectivesModule
  ],
  entryComponents: [
    SearchModal,
    SearchResultPage,
    TicketSubmissionFormPage,
    LocationFilterModal
  ],
  providers: [
    HomeService
  ]
})
export class HomePageModule {}
