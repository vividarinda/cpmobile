export class HomeModel{
  customer: CustomerModel = new CustomerModel();
  timeline: TimelineModel[] = [];
  equipments: EquipmentsModel[] = [];
}

export class CustomerModel{
  customerName: string;
  customerId: string;
  tickets: number;
  orders: number;
}

export class EquipmentsModel{
  brandCode: string;
  brandName: string;
  brandImagePath: string;
  amount: number;
  order: number;
}

export class TimelineModel{
  title: string;
  content: string;
  icon: string;
  time: string;
}