import { Component, ViewChild } from '@angular/core';
import { TicketSubmissionFormService } from './ticket-submission-form.service';

@Component({
  selector: 'page-ticket-submission-form',
  templateUrl: 'ticket-submission-form.component.html',
  providers: [TicketSubmissionFormService]
})
export class TicketSubmissionFormPage {
  
}
