export class TicketSubmissionFormModel{
  name: string;
  phoneNumber: string;
  email: string;
  case: string;
  call: string;
  subcall: string;
  location: string;
  date: Date;
  desc: string;
}