import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
@Component({
  selector: 'location-filter',
  templateUrl: 'location-filter.component.html'
})
export class LocationFilterModal {
  public orderStatus = 'openorder';
  public period = 'monthtodate';
  public location = "All Location";

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
    this.location = this.navParams.data.selectedLocation;
  }

  public onClickCloseButton(){
    this.viewCtrl.dismiss();
  }

  public onClickLocation(loc: string){
    this.location = loc;
    // return this.location;
    this.viewCtrl.dismiss({location:this.location});
  }

}
