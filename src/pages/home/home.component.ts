import { HomeModel } from './home.model';
import { HomeService } from './home.service';
import { Component, ViewChild } from '@angular/core';
import { NavController, IonicPage, Slides, ModalController, Events } from 'ionic-angular';
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import { MyEquipmentsPage } from '../my-equipments/my-equipments.component';
import { CONSTANTS } from '../../common/constants/constants';
import { LoadingService } from '../../services/loading.service';
import { SearchModal } from './search/search.component';
import { LocationFilterModal } from './location-filter/location-filter.component';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.component.html',
  providers: [HomeService]
})
export class HomePage {
  @ViewChild('mySlider') slider: Slides;
  public model = new HomeModel();
  public subscriptions: Subscription[] = [];
  public sliderStatus = 0;
  public selectedLocation = "All Location";

  constructor(public navCtrl: NavController, 
              private homeService: HomeService, 
              private loadingService: LoadingService,
              private modalCtrl: ModalController,
              private events: Events) {
    this.selectedLocation = "All Location";
  }

  ionViewDidLoad(){
    this.subscribeCustomerData();
    this.subscribeTimelineData();
    this.subscribeEquipmentsData();
    this.slider.ionSlideProgress.subscribe(progress => this.onSliderProgress(progress));
  }

  private subscribeCustomerData(){
    this.subscriptions.push(
      this.homeService.getCustomerData().timeout(CONSTANTS.API.TIME_OUT_VAL).subscribe(
        result => {
          this.loadingService.dismissLoading();
          if(result != null){
            this.model.customer = result;
          }
      }, error => {
        this.loadingService.dismissLoading();
      })
    )
  }

  private subscribeTimelineData(){
    this.subscriptions.push(
      this.homeService.getTimelineData().subscribe(
        result => {
          if(result != null){
            this.model.timeline = result;
          }
      }, error => {
        this.loadingService.dismissLoading();
        //this.alertService.showError(error.message);
      })
    )
  }

  private subscribeEquipmentsData(){
    this.homeService.getEquipmentData("10611").subscribe(
      result => { if(result != null) {
          this.model.equipments = result;
        }
      }
    );
  }

  private getByBrand(brandName: string){
    let amount = 0;
    if(this.model.equipments.find(item => item.brandName == brandName)){
      amount = this.model.equipments.find(item => item.brandName == brandName).amount;
    }
    return amount;
  }

  public onClickAddTicket(){
    console.log('clicked');
    this.navCtrl.push('MyOrdersPage');
  }

  public onClickEquipment(brand: string){
    brand = brand == "ALL" ? "" : brand;
    let brandCode = null;
    if(this.model.equipments.find(item => item.brandName == brand)){
      brandCode = this.model.equipments.find(item => item.brandName == brand).brandCode;
    }
    //this.navCtrl.push(MyEquipmentsPage, {brand: brand, brandCode: brandCode});
    this.events.publish('change-tab', 1, {brand: brand, brandCode: brandCode});
  }

  public onClickSearch(){
    console.log('clicked');
    const modal = this.modalCtrl.create(SearchModal, 
      null, {cssClass: 'modal-search', showBackdrop: true, enterAnimation: 'enter-fade', leaveAnimation: 'leave-fade'});
    modal.present();  
  }

  onSliderProgress(progress) {
    this.sliderStatus = progress;
    //console.log(progress);
  }

  onClickLocationFilter(){
    console.log('location filter clicked');
    const modal = this.modalCtrl.create(LocationFilterModal, 
      {selectedLocation: this.selectedLocation}, {cssClass: 'modal-orders-sort', showBackdrop: true});
    modal.onDidDismiss(selectedLocation => {
      if(selectedLocation){
        console.log(selectedLocation);
        this.selectedLocation = selectedLocation.location;
      }
    });
    modal.present(); 
  }
}
