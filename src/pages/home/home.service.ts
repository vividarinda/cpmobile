import { DataService } from './../../services/data.service';
import { CONSTANTS } from './../../common/constants/constants';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HomeService {

  constructor(private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }

  public getCustomerData(): Observable<any>{
    return this.dataService.get(`https://moqmulatorv2.azurewebsites.net/getCustomer`);
  }

  public getTimelineData(): Observable<any>{
    return this.dataService.get(`https://moqmulatorv2.azurewebsites.net/getTimeline`);
  }

  public getEquipmentData(customerCode: string): Observable<any>{
    return this.dataService.get(`${CONSTANTS.API.BASE_URL}api/equipment/dashboard?customerCode=${customerCode}`);
  }
}
