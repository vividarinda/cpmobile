import { async, TestBed } from '@angular/core/testing';
import { HomePage } from './home.component';
import { IonicPageModule, NavController, LoadingController } from 'ionic-angular';
import { TimelineModule } from '../../components/timeline/timeline.module';
import { DirectivesModule } from '../../directives/directives.module';
import { HomeService } from './home.service';
import { HomeServiceMock, NavMock, DataServiceMock } from '../../../test-config/mocks-ionic';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { LoadingService } from '../../services/loading.service';
import { DataService } from '../../services/data.service';
import { TestUtils } from '../../../test-config/test-utils';

describe('Home Component', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestUtils.configureIonicTestingModule();
    TestBed.configureTestingModule({
        declarations: [HomePage],
        imports: [
            IonicPageModule.forChild(HomePage),
            TimelineModule,
            DirectivesModule
        ],
        providers: [
          { provide: NavController, useClass: NavMock },
          { provide: HomeService, useClass: HomeServiceMock },
          { provide: DataService, useClass: DataServiceMock },
          NativePageTransitions,
          LoadingService,
          LoadingController
        ]
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component instanceof HomePage).toBe(true);
  });
});