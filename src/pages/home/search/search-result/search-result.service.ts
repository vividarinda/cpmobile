import { Injectable } from '@angular/core';
import { DataService } from '../../../../services/data.service';
import { CONSTANTS } from '../../../../common/constants/constants';
import { Observable } from 'rxjs';

@Injectable()
export class SearchResultService {

  constructor(private dataService: DataService) {
    console.log('MyEquipmentStatus Service called');
  }

  public getAllEquipmentsByStatus(): Observable<any>{
    return this.dataService.get(CONSTANTS.API.BASE_URL + 'myequipment/statusdetail/unitcaution');
  }
}