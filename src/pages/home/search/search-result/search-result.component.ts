import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';
import { LoadingService } from '../../../../services/loading.service';
import { SearchResultModel } from './search-result.model';
import { SearchResultService } from './search-result.service';

@Component({
    selector: 'search-result',
    templateUrl: 'search-result.component.html',
    providers: [SearchResultService]
})
export class SearchResultPage {
    public model = new SearchResultModel();
    
    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController,
        private searchResultService: SearchResultService,
        private navParams: NavParams,
        private loadingService: LoadingService) {
        
    }
}