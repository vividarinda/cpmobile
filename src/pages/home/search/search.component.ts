import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

@Component({
    selector: 'modal-search',
    templateUrl: 'search.component.html',
})
export class SearchModal {
    @ViewChild('input') myInput ;
    searchString: string;

    constructor(
        public navCtrl: NavController,
        public modalCtrl: ModalController) {

    }

    ionViewDidLoad() {
        setTimeout(() => {
            this.myInput.setFocus();
        }, 500);
    }

    public onClickGoBack(){
        this.navCtrl.pop();
    }

    public onSearch(){
        console.log(this.searchString);
    }
}