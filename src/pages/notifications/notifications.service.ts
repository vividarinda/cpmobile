import { CONSTANTS } from './../../common/constants/constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { map } from '../../../node_modules/rxjs/operators';
import { BehaviorSubject } from '../../../node_modules/rxjs/BehaviorSubject';
import { DataService } from './../../services/data.service';
import { Observable } from 'rxjs';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NotificationsService {

  constructor(private http: HttpClient, private dataService: DataService) {
    console.log('Hello RestProvider Provider');
  }

  public getTimelineData(): Observable<any>{
    return this.dataService.get(`https://moqmulatorv2.azurewebsites.net/getTimeline`);
  }
}
