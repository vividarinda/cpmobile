export class NotificationsModel{
    timeline: TimelineModel[] = [];
  }
  
  export class TimelineModel{
    title: string;
    content: string;
    icon: string;
    time: string;
  }