import { NotificationsPage } from './notifications.component';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationsService } from './notifications.service';
import { NotificationModule } from '../../components/notification/notification.module';

@NgModule({
  declarations: [
    NotificationsPage
  ],
  imports: [
    IonicPageModule.forChild(NotificationsPage),
    NotificationModule
  ],
  providers: [
    NotificationsService
  ]
})
export class NotificationsPageModule {}
