import { NotificationsService } from './notifications.service';
import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import { NotificationsModel } from './notifications.model';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.component.html',
  providers: [NotificationsService]
})
export class NotificationsPage {
  
  public subscriptions: Subscription[] = [];
  public model = new NotificationsModel();
  
  constructor(public navCtrl: NavController, private notificationsService: NotificationsService) {
    
  }

  ionViewDidLoad(){
    this.subscribeTimelineData();
  }

  private subscribeTimelineData(){
    this.subscriptions.push(
      this.notificationsService.getTimelineData().subscribe(
        result => {
          if(result != null){
            this.model.timeline = result;
          }
      }, error => {
        // this.loadingService.dismissLoading();
        //this.alertService.showError(error.message);
      })
    )
  }
}
