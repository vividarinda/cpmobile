import { Component, ViewChild } from '@angular/core';
import { Events, Tabs } from 'ionic-angular';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('tabs') tabs: Tabs;
  tab1Root = 'HomePage';
  tab2Root = 'MyEquipmentsPage';
  tab3Root = 'MyOrdersPage';
  tab4Root = 'NotificationsPage';
  tab5Root = 'OthersPage';

  public tabParams = {
    params: {
      brand: "",
      brandCode: ""
    }
  };

  constructor(private events: Events) {
    this.events.subscribe('change-tab', (tab, params) => {
      this.tabParams.params = params;
      console.log(this.tabParams);
      this.tabs.select(tab);
      //this.tabs.getSelected().
      //this.tabs.getSelected().popToRoot();
    });
  }
}
