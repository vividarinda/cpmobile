import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { LandingPage } from './landing.component';
import { LoginPage } from './login/login.component';
import { ForgotPasswordModal } from './login/forgot-password-modal/forgot-password-modal.component';
import { RegisterPage } from './register/register.component';

@NgModule({
  declarations: [
    LandingPage,
    LoginPage,
    RegisterPage,
    ForgotPasswordModal
  ],
  imports: [
    IonicPageModule.forChild([LandingPage, RegisterPage])
  ],
  entryComponents: [
    LoginPage,
    RegisterPage,
    ForgotPasswordModal
  ],
  providers: [
   
  ]
})
export class LandingPageModule {}
