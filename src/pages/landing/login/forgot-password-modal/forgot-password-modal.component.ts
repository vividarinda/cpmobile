import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ForgotPasswordModalService } from './forgot-password-modal.service';

@Component({
  selector: 'modal-forgot-password',
  templateUrl: 'forgot-password-modal.component.html',
  providers: [ForgotPasswordModalService]
})
export class ForgotPasswordModal {

  public email = '';

  constructor(public navCtrl: NavController, 
              private forgotPasswordModalService: ForgotPasswordModalService) {
    
  }

  public onClickCancel(){
    this.navCtrl.pop();
  }
}