import { Component } from '@angular/core';
import { NavController, IonicPage, ModalController } from 'ionic-angular';
import { LoginService } from './login.service';
import { LoginModel } from './login.model';
import { ForgotPasswordModal } from './forgot-password-modal/forgot-password-modal.component';
import { TabsPage } from '../../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.component.html',
  providers: [LoginService]
})
export class LoginPage {

  public model = new LoginModel();
  public password = '';
  constructor(public navCtrl: NavController, 
              private loginService: LoginService,
              private modalController: ModalController) {
    
  }

  public onClickLogin(){
    this.navCtrl.push(TabsPage);
  }

  public onClickGoBack(){
    this.navCtrl.pop();
  }

  public onClickEye(){
    this.model.isPasswordVisible = !this.model.isPasswordVisible;
  }

  public onClickForgotPassword(){
    this.modalController
      .create(ForgotPasswordModal, null, {cssClass: 'modal-forgot-password', showBackdrop: true})
      .present();
  }
}