export class RegisterModel{
  email = '';
  password = '';
  username = '';
  fullname = '';
  customerCode = '';
  progress = '25%';
  confirmPassword = '';
  isPasswordVisible = false;
  isConfirmPasswordVisible = false;
}
