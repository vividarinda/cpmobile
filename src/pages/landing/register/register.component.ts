import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, Slides } from 'ionic-angular';
import { TabsPage } from '../../tabs/tabs';
import { RegisterModel } from './register.model';
import { RegisterService } from './register.service';

@Component({
  selector: 'page-register',
  templateUrl: 'register.component.html',
  providers: [RegisterService]
})
export class RegisterPage {
  @ViewChild(Slides) slides: Slides;
  public model = new RegisterModel();
  constructor(public navCtrl: NavController, 
              private registerService: RegisterService,
              private modalController: ModalController) {
    
  }

  ionViewDidLoad(){
    this.slides.lockSwipes(true);
  }

  public onClickLogin(){
    this.navCtrl.push(TabsPage);
  }

  public onClickGoBack(){
    if(this.slides.isBeginning()){
      this.navCtrl.pop();
    }else{
      this.slidePrev();
    }
  }

  public onClickShowPassword(){
    this.model.isPasswordVisible = !this.model.isPasswordVisible;
  }

  public onClickShowConfirmPassword(){
    this.model.isConfirmPasswordVisible = !this.model.isConfirmPasswordVisible;
  }

  public onClickNext(){
    this.slideNext();
  }

  public ionSlideDidChange(){
    this.model.progress = ((this.slides.realIndex + 1) * 25) + '%';
  }

  public onClickSend(){
    this.slideNext();
  }

  private slideNext(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  private slidePrev(){
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  public onClickComplete(){
    this.navCtrl.setRoot(TabsPage);
  }
}