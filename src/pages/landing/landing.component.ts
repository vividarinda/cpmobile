import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { LoginPage } from './login/login.component';
import { RegisterPage } from './register/register.component';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.component.html'
})
export class LandingPage {

  constructor(public navCtrl: NavController) {
    
  }

  public onClickLogin(){
    this.navCtrl.push(LoginPage);
  }

  public onClickRegister(){
    this.navCtrl.push(RegisterPage);
  }
}
