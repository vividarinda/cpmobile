import { Injectable } from '@angular/core';
import { AlertController, ToastController } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SnackbarService {

    isSnackbarPresent = false;

    constructor(private toastController: ToastController) {
        console.log('Hello RestProvider Provider');
    }

    showSnackbar(errorMessage: string){
        if(!this.isSnackbarPresent){
            let toast = this.toastController.create({
                message: errorMessage,
                position: 'bottom',
                cssClass: 'snackbar',
                showCloseButton: true,
                closeButtonText: 'Dismiss'
            });
            toast.onDidDismiss(() => {
                this.isSnackbarPresent = false;
            });
            toast.present();
            this.isSnackbarPresent = true;
        }
    }
}
