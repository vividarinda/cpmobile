import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AlertService {
  constructor(private alertController: AlertController) {
    console.log('Hello RestProvider Provider');
  }

  showError(errorMessage: string){
    let alert = this.alertController.create({
        title: 'Error',
        subTitle: errorMessage,
        buttons: ['Dismiss']
    });
    alert.present();
  }
}
