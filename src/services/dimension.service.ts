import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DimensionService {

    private screenWidth: number;
    private screenHeight: number;
  
    constructor() {
        console.log('Hello RestProvider Provider');
    }

    public getScreenWidth(){
        return this.screenWidth;
    }

    public getScreenHeight(){
        return this.screenHeight;
    }

    public setScreenWidth(width: number){
        this.screenWidth = width;
    }

    public setScreenHeight(height: number){
        this.screenHeight = height;
    }
}
