import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, timeout } from 'rxjs/operators';
import { share } from 'rxjs/operators/share';
import { LoadingService } from './loading.service';
import { SnackbarService } from './snackbar.service';
import { CONSTANTS } from '../common/constants/constants';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataService {
  constructor(private storage: Storage, private http: HttpClient, private loading: LoadingService, private snackbarService: SnackbarService) {
    console.log('Hello RestProvider Provider');
  }

  get(url: string, params: HttpParams = new HttpParams()): Observable<any> {
    // this.loading.showLoading();
    let request = this.http.get(url, { params }).pipe(timeout(500000), catchError(this.formatErrors));
    return this.loadData(url, request);
  }

  put(url: string, body: Object = {}): Observable<any> {
    // this.loading.showLoading();
    return this.http.put(url, body).pipe(catchError(this.formatErrors));
  }

  post(url: string, body: Object = {}): Observable<any> {
    // this.loading.showLoading();
    return this.http.post(url,body).pipe(catchError(this.formatErrors));
  }

  delete(url): Observable<any> {
    // this.loading.showLoading();
    return this.http.delete(url).pipe(catchError(this.formatErrors));
  }

  private formatErrors(error: any) {
    if(error.status == 0){
      error.message = 'Network Error';
    }
    return  new ErrorObservable(error);
  }

  loadData(key: string, observable: Observable<any>): Observable<any> {
    const observableSubject = new Subject<any>();
    observable = observable.pipe(share());

    const subscribeOrigin = () => {
      observable.subscribe(
        res => {
          this.storage.set(key, res);
          observableSubject.next(res);
          observableSubject.complete();
        },
        err => {
          this.snackbarService.showSnackbar(err.status == 0 ? CONSTANTS.ERROR[''+err.status] : err.message);
          observableSubject.error(err);
        },
        () => {
          observableSubject.complete();
        }
      );
    };

    console.log(key);

    this.storage.get(key)
      .then(data => {
        observableSubject.next(data);
        subscribeOrigin();
      })
      .catch(e => {
        this.storage.get(key)
          .then(res => {
            observableSubject.next(res);
            subscribeOrigin();
          })
          .catch(() => subscribeOrigin());
      });

    return observableSubject.asObservable();
  }
}
