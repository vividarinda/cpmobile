import { Injectable } from '@angular/core';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TabsParamService {
  private params: any;
  
  constructor(){
    console.log('Hello RestProvider Provider');
  }

  public set(params: any){
    this.params = params;
  }

  public get(){
    return this.params;
  }
}
