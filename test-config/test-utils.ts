import { TestBed } from "@angular/core/testing";
import { IonicModule, Platform } from "ionic-angular";
import { MyApp } from "../src/app/app.component";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Push } from "@ionic-native/push";
import { StatusBarMock, SplashScreenMock, PlatformMock } from "./mocks-ionic";


export class TestUtils {
    public static configureIonicTestingModule(): typeof TestBed {
      return TestBed.configureTestingModule({
        declarations: [MyApp],
        imports: [
          IonicModule.forRoot(MyApp)
        ],
        providers: [
          { provide: StatusBar, useClass: StatusBarMock },
          { provide: SplashScreen, useClass: SplashScreenMock },
          { provide: Platform, useClass: PlatformMock },
          Push
        ]
      });
    }
  
    // http://stackoverflow.com/questions/2705583/how-to-simulate-a-click-with-javascript
    public static eventFire(el: any, etype: string): void {
      if (el.fireEvent) {
        el.fireEvent('on' + etype);
      } else {
        let evObj: any = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
      }
    }
  }